from tkinter import *
import speech_recognition as sr
from collections import Counter
import re, string

r = sr.Recognizer()

#Método que calcula la cantidad de palabras enlace
def conectores(texto):
    texto.lower()
    listaPalabras = texto.split()
    frecuenciaPalab = []
    total = 0
    for w in listaPalabras:
        if( w == "el" ):
            total+=1
        elif(w == "y"):
            total+=1
        elif(w == "en"):
            total += 1
        elif(w == "la"):
            total += 1
        elif(w == "o"):
            total += 1
        elif(w == "los"):
            total += 1
        elif(w == "con"):
            total += 1
        elif(w == "de"):
            total += 1
        elif(w == "a"):
            total += 1
        elif(w == "su"):
            total += 1
        elif(w == "que"):
            total += 1
        elif(w == "un"):
            total += 1
        elif(w == "una"):
            total += 1
        elif(w == "las"):
            total +=1
        elif(w == "es"):
            total +=1
        elif(w == "por"):
            total +=1
        elif(w == "te"):
            total +=1
        elif(w == "lo"):
            total+=1
        elif(w == "se"):
            total+=1
        elif(w == "ha"):
            total +=1
        elif(w == "del"):
            total+=1
        elif(w == "al"):
            total+=1
        elif(w == "era"):
            total+=1
    #txt2.config(state=ENABLED)
    a = "\nTotal de conectores: "+str(total)
    txt2.insert(INSERT, a)
lista =  [" el "," y ", " en ", " la ", " o ", " los ", " con ", " de ",  " a ", " su " , " que ", " un ", " una ", " las ", " es ", " por ", " te ", " lo ", " se ", " no ", " ha ", " del ", " le ", " al ", " era "]

#Método que calcula la cantidad de caracteres especiales
def carEs(texto):
    listaPalabras = texto.split()
    frecuenciaPalab = []
    total = 0
    for w in texto:
        if(w == "."):
            total+=1
        elif(w == ","):
            total+=1
        elif(w == "-"):
            total+=1
        elif(w == ":"):
            total+=1
        elif(w == "¿"):
            total+=1
        elif(w == "?"):
            total+=1
        elif(w == "!"):
            total+=1
        elif(w == "¡"):
            total+=1
    a ="\nTotal de signos de puntuacion: "+str(total)
    txt2.insert(INSERT, a)

#Método que calcula la cantidad de palabras en el texto
def frecuencias(texto):
    texto2 = remove_punctuation(texto.lower())
    listaPalabras = texto2.split()
    a = 0;
    for i in listaPalabras:
        a+=1
    b ="\nTotal de palabras: "+str(a)
    txt2.insert(INSERT, b)

#Método que calcula la frecuancia con que se repiten las palabras
def res():
    texto=txt1.get("1.0","end-1c") #se extrae el texto
    texto2 = remove_punctuation(texto.lower()) #se remueven los caracteres especiales y se pasa a minusculas
    #se eliminan las palabras enlace
    for i in lista:
        texto3 = texto2.replace(i," ")
        texto2 = texto3

    list1= texto3.split() #dividimos el texto en palabras
    counts = Counter(list1) #se cuenta la frecuancia de las palabras en un diccionario
    nume = []
    #se obtiene las claves del diccionario
    for key in counts:
        nume.append(counts[key]) #3,4,9,10

    #se calcuala el numero mayor de la lista creada a paritir de las claves del diccionario
    for y in range(4):
        n_mayor = 0
        k = 0
        #numero mayor
        for i in nume:
            n = nume[k]
            if n>n_mayor:
                n_mayor = n
            else:
                n_mayor = n_mayor
            k = k+1
        a = ""
        int = 0
        txt = ""

        #se busca el numero mayor en el diccionario y se obtiene el valor
        for k, v in counts.items():
            if(v==n_mayor):
                #se crea una cadena con todas las palabras obtenidas
                txt = txt +" "+k
                if(y == 0):
                    #se muestra la cadena en la etiqueta
                    eti1.configure(text = txt)
                    int+=1
                    a = k
                if(y == 1):
                    eti2.configure(text = txt)
                    int+=1
                    a = k
                if(y == 2):
                    eti3.configure(text = txt)
                    int+=1
                    a = k
                if(y == 3):
                    eti4.configure(text = txt)
                    int+=1
                    a = k
        #se elimina de la lista y el diccionario el numero mayor junto con su valor, esto eviatara que vuela a aparecer
        for i in range(int):
            del counts[a]
            nume.remove(n_mayor)

#Metodos que elimina los caracteres especiales
def remove_punctuation ( text ):
  return re.sub('[%s]' % re.escape(string.punctuation), ' ', text)

#Metodo que permite convertir voz a texto
def voz():
    lbl2.configure(text = "Escuchando", fg = "green")
    with sr.Microphone() as source:
        print('Di tu nombre : ')
        audio = r.listen(source)
    try:
        #a = 1
        text = r.recognize_google(audio)
        print('Dijiste: {}'.format(text))
        txt1.insert(INSERT, text)
        lbl2.configure(text = "Listo", fg = "orange")
    except:
        lbl2.configure(text = "No se entendio", fg = "red")

def texto():
    a=txt1.get("1.0","end-1c")
    frecuencias(a)
    conectores(a)
    carEs(a)

ventana = Tk() #creamos la ventana
ventana.title("Resume el texto") #agregamos titulo a la ventana
ventana.resizable(0,0) #no se puede cambiar el tamaño
ventana.geometry("800x500") #tamaño de la ventana

lbl1 = Label(ventana, text="Escribe un texto o ") #creamos una etiqueta con texto
lbl1.place(x=10, y=10) #indicamos una posicion en la ventana

lbl2 = Label (ventana)
lbl2.place(x=155, y = 10 )

txt1 = Text(ventana)
txt1.place(x=10, y = 40, width=780, height = 150)

btn1 = Button(ventana, text="dictar", command=lambda: voz())
btn1.place(x=110, y=8)

btn2 = Button(ventana, text="Resumir", command=lambda: texto())
btn2.place(x= 10, y = 200)

txt2 = Text(ventana)
txt2.place(x = 10, y= 230, width = 380, height = 150)
txt2.configure(bg = "snow2")

btn3 = Button(ventana, text="Nube de Palabras", command=lambda: res())
btn3.place(x= 410, y = 200)

eti1 = Label(ventana)
eti1.place(x = 410, y = 230, width=380, height = 50)
eti1.config(font=("Verdana",25))

eti2 = Label(ventana)
eti2.place(x = 410, y = 290, width=380, height = 50)
eti2.config(font=("Verdana",17))

eti3 = Label(ventana)
eti3.place(x = 410, y = 350, width=380, height = 50)
eti3.config(font=("Verdana",13))

eti4 = Label(ventana)
eti4.place(x = 410, y = 410, width=380, height = 50)
eti4.config(font=("Verdana",8))
ventana.mainloop();
